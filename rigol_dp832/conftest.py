def pytest_addoption(parser):
    parser.addoption(
        "--channel",
        action="store",
        default="1",
        type=int,
        help="Channel number to test against",
    )


def pytest_generate_tests(metafunc):
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixturenames".
    option_value = metafunc.config.option.channel
    if "channel" in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("channel", [option_value])
