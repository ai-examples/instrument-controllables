# Used for tests
import pytest
import time

from rigol_dp832 import rigol_dp


class DP_Test:
    """
    Contains tests for the Rigol DP power supply series to ensure functionality
    such as output mode setting, OCP alarm status, and alarm clearing operates as expected.
    """

    def setup_class(self):
        """
        Prepares the testing environment by turning off the output for all channels.

        This method is called before starting the tests to ensure a consistent starting state.
        """
        for channel in self.CHANNELS:
            self.ps.set_output_state(channel, False)

    def teardown_class(self):
        """
        Cleans up the testing environment by turning off the output for all channels.

        This method is called after all tests have run to ensure the device is left in a safe state.
        """
        for channel in self.CHANNELS:
            self.ps.set_output_state(channel, False)

    def test_get_output_mode(self, channel: int):
        """
        Verifies that the default state for the output mode of a specified channel is Constant Voltage (CV).

        Parameters:
        - channel (int): The power supply channel to be tested.

        Asserts:
        - The output mode for the given channel is "CV".
        """
        assert self.ps.get_output_mode(channel) == "CV"

    def test_get_ocp_alarm(self, channel: int):
        """
        Checks whether the Over Current Protection (OCP) alarm has not been triggered for a specified channel.

        Parameters:
        - channel (int): The power supply channel to be tested.

        Asserts:
        - The OCP alarm status for the given channel is False, indicating no alarm.
        """
        assert self.ps.get_ocp_alarm(channel) == False

    def test_clear_ocp_alarm(self, channel: int):
        """
        Ensures that clearing the Over Current Protection (OCP) alarm for a specified channel returns None,
        indicating the command was received and processed successfully.

        Parameters:
        - channel (int): The power supply channel for which the OCP alarm is to be cleared.

        Asserts:
        - Clearing the OCP alarm for the given channel returns None.
        """
        assert self.ps.clear_ocp_alarm(channel) == None

    def test_ocp_enabled(self, channel: int):
        """
        Verifies the functionality of enabling and disabling Over Current Protection (OCP)
        for a specified channel and ensures the setting is accurately reflected.

        Parameters:
        - channel (int): The power supply channel to test OCP enable/disable functionality on.

        Steps:
        1. Enables OCP and asserts the setting is enabled (True).
        2. Disables OCP and asserts the setting is disabled (False).
        """
        self.ps.set_ocp_enabled(channel, True)
        assert self.ps.get_ocp_enabled(channel) == True
        self.ps.set_ocp_enabled(channel, False)
        assert self.ps.get_ocp_enabled(channel) == False

    def test_ocp_value(self, channel: int):
        """
        Sets the Over Current Protection (OCP) value for a specified channel and verifies
        if the set value is accurately stored and retrieved.

        Parameters:
        - channel (int): The power supply channel to test setting and getting the OCP value.
        - ocp_value (float): The value to set for OCP.

        Steps:
        1. Sets a specific OCP value.
        2. Asserts that the retrieved OCP value matches the set value.
        """
        self.ps.set_ocp_value(channel, 3.3)
        assert self.ps.get_ocp_value(channel) == 3.3

    def test_get_ovp_alarm(self, channel: int):
        """
        Checks whether the Over Voltage Protection (OVP) alarm has not been triggered
        for a specified channel.

        Parameters:
        - channel (int): The power supply channel to check the OVP alarm status for.

        Asserts:
        - The OVP alarm status for the given channel is False, indicating no alarm.
        """
        assert self.ps.get_ovp_alarm(channel) == False

    def test_clear_ovp_alarm(self, channel: int):
        """
        Ensures that clearing the Over Voltage Protection (OVP) alarm for a specified channel
        returns None, indicating the command was received and processed successfully.

        Parameters:
        - channel (int): The power supply channel for which the OVP alarm is to be cleared.

        Asserts:
        - Clearing the OVP alarm for the given channel returns None.
        """
        assert self.ps.clear_ovp_alarm(channel) == None

    def test_ovp_enabled(self, channel: int):
        """
        Verifies the functionality of enabling and disabling Over Voltage Protection (OVP)
        for a specified channel and ensures the setting is accurately reflected.

        Parameters:
        - channel (int): The power supply channel to test OVP enable/disable functionality on.

        Steps:
        1. Enables OVP and asserts the setting is enabled (True).
        2. Disables OVP and asserts the setting is disabled (False).
        """
        self.ps.set_ovp_enabled(channel, True)
        assert self.ps.get_ovp_enabled(channel) == True
        self.ps.set_ovp_enabled(channel, False)
        assert self.ps.get_ovp_enabled(channel) == False

    def test_ovp_value(self, channel: int):
        """
        Sets the Over Voltage Protection (OVP) value for a specified channel and verifies
        if the set value is accurately stored and retrieved.

        Parameters:
        - channel (int): The power supply channel to test setting and getting the OVP value.
        - ovp_value (float): The value to set for OVP.

        Steps:
        1. Sets a specific OVP value.
        2. Asserts that the retrieved OVP value matches the set value.
        """
        self.ps.set_ovp_value(channel, 5)
        assert self.ps.get_ovp_value(channel) == 5

    def test_output_state(self, channel: int):
        """
        Tests the functionality to turn the output state on or off for a specified channel and verifies the state.

        Parameters:
        - channel (int): The power supply channel to toggle the output state on or off.

        Steps:
        1. Turns the output state on and verifies it is on (True).
        2. Turns the output state off and verifies it is off (False).
        """
        self.ps.set_output_state(channel, True)
        assert self.ps.get_output_state(channel) == True
        self.ps.set_output_state(channel, False)
        assert self.ps.get_output_state(channel) == False

    def test_channel_settings(self, channel: int):
        """
        Sets and verifies the voltage and current settings for a specified channel.

        Parameters:
        - channel (int): The power supply channel to set the voltage and current for.

        Asserts:
        - The voltage and current settings match the specified values.
        """
        VOLTAGE = 5
        CURRENT = 1
        self.ps.set_channel_settings(channel=channel, voltage=VOLTAGE, current=CURRENT)
        channel_settings = self.ps.get_channel_settings(channel)
        assert int(channel_settings["voltage"]) == VOLTAGE
        assert int(channel_settings["current"]) == CURRENT

    def test_measure(self, channel: int):
        """
        Sets voltage and current for a channel, turns on the power supply, and then measures
        the voltage and current to verify they are within expected ranges.

        Parameters:
        - channel (int): The power supply channel to perform the measurements on.

        Steps:
        1. Sets voltage and current, then turns on the power supply.
        2. Verifies the measured voltage and current are within expected ranges.
        3. Additionally verifies the 'measure_all' function's output for voltage, current, and power.
        4. Turns off the power supply.
        """
        VOLTAGE = 5
        CURRENT = 1
        self.ps.set_channel_settings(channel=channel, voltage=VOLTAGE, current=CURRENT)
        self.ps.set_output_state(channel, True)
        time.sleep(1)  # Wait for the power supply to stabilize

        # Verify expected voltage and current within a tolerance range
        assert 0.99 * VOLTAGE < self.ps.measure_voltage(channel) < VOLTAGE * 1.01
        assert self.ps.measure_current(channel) != 0

        # Verify 'measure_all' functionality
        all = self.ps.measure_all(channel)
        assert 0.99 * VOLTAGE < all["voltage"] < VOLTAGE * 1.01
        assert all["current"] != 0
        assert all["power"] != 0

        self.ps.set_output_state(channel, False)


class DP832_Test(DP_Test):
    """
    Defines tests specific to the Rigol DP832 power supply model, including functionality
    such as instrument identification and channel-specific tests inherited from DP_Test.
    """

    CHANNELS = [1, 2, 3]

    def test_id(self):
        """
        Verifies the identification string of the instrument to ensure it matches the expected
        manufacturer and model details.

        Asserts:
        - The manufacturer is "RIGOL TECHNOLOGIES".
        - The model is "DP832A".
        """
        id = self.ps.id()
        assert id["manufacturer"] == "RIGOL TECHNOLOGIES"
        assert id["model"] == "DP832A"


@pytest.mark.skip("Timeout when running tests but when using class objects manually.")
class Test_DP832_USBTMC(DP832_Test):
    """
    A test suite for the Rigol DP832 power supply using USBTMC connection.

    Note: Tests are skipped due to timeout issues when running automatically, but manual
    usage of class objects is functional.
    """

    ps = rigol_dp.DP832(usb_conn_type="USBTMC", vid=0x1AB1, pid=0x0E11)


class Test_DP832_VISA(DP832_Test):
    """
    A test suite for the Rigol DP832 power supply using VISA connection.

    This suite tests the functionality of the DP832 power supply over a VISA connection,
    specifying the VID and PID for device identification.
    """

    ps = rigol_dp.DP832(
        usb_conn_type="VISA", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DP8"
    )
