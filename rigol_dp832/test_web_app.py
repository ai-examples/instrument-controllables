from fastapi.testclient import TestClient
from rigol_dp832.web_app import app

client = TestClient(app)


class TestDP832API:
    def test_read_root(self):
        response = client.get("/")
        assert response.status_code == 200
        assert response.json() == {"status": "OK"}

    def test_get_device_id(self):
        response = client.get("/id/")
        assert response.status_code == 200
        # The exact response will depend on your specific device's ID info
        assert "manufacturer" in response.json()

    def test_set_channel_settings(self):
        # This test will actually change the channel settings on your device
        response = client.post(
            "/set-channel-settings/",
            json={"channel": 1, "voltage": 5.0, "current": 1.0},
        )
        assert response.status_code == 200
        assert response.json() == {"message": "Channel settings updated successfully"}

    def test_set_output_state(self):
        # This will actually enable or disable the output state of the specified channel
        response = client.post("/set-output-state/", json={"channel": 1, "state": True})
        assert response.status_code == 200
        assert response.json() == {"message": "Output state updated successfully"}

    def test_measure_current(self):
        # Assumes the current can be measured at the given settings
        response = client.get("/measure-current/?channel=1")
        assert response.status_code == 200
        # Validate against expected current if known, or just check for key presence
        assert "current" in response.json()

    def test_measure_voltage(self):
        # Assumes the voltage can be measured at the given settings
        response = client.get("/measure-voltage/?channel=1")
        assert response.status_code == 200
        # Validate against expected voltage if known, or just check for key presence
        assert "voltage" in response.json()
