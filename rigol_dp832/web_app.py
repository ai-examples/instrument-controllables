# Append parent path for includes
import os, sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from fastapi import FastAPI, HTTPException, Query
from pydantic import BaseModel

from rigol_dp832.rigol_dp import DP832

app = FastAPI()

# Initialize the device connection once and use it across requests
device = DP832(usb_conn_type="VISA", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DP8")


@app.get("/")
def read_root():
    """
    Root endpoint for basic API health check.

    Returns:
        dict: A simple status message indicating the API is operational.
    """
    return {"status": "OK"}


@app.get("/id/")
def get_device_id():
    """
    Retrieves the device ID from the connected device.

    Returns:
        dict: The ID information of the device.

    Raises:
        HTTPException: If any error occurs during device communication.
    """
    try:
        return device.id()
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


# Define Pydantic models for the request bodies
class ChannelSettings(BaseModel):
    channel: int
    voltage: float
    current: float


class OutputState(BaseModel):
    channel: int
    state: bool


@app.post("/set-channel-settings/")
def set_channel_settings(settings: ChannelSettings):
    """
    Sets the voltage and current settings for a specified channel.

    Args:
        settings (ChannelSettings): The desired settings including channel, voltage, and current.

    Returns:
        dict: A message indicating successful update of channel settings.

    Raises:
        HTTPException: If any error occurs during the operation.
    """
    try:
        device.set_channel_settings(
            settings.channel, settings.voltage, settings.current
        )
        return {"message": "Channel settings updated successfully"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/set-output-state/")
def set_output_state(settings: OutputState):
    """
    Enables or disables the output state of a specified channel.

    Args:
        settings (OutputState): The desired state including channel and boolean state.

    Returns:
        dict: A message indicating successful update of the output state.

    Raises:
        HTTPException: If any error occurs during the operation.
    """
    try:
        device.set_output_state(settings.channel, settings.state)
        return {"message": "Output state updated successfully"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.get("/measure-current/")
def measure_current(
    channel: int = Query(
        default=1, description="Channel number to measure current from"
    )
):
    """
    Measures the current from a specified channel.

    Args:
        channel (int, optional): The channel number from which to measure current. Defaults to 1.

    Returns:
        dict: The current measurement for the specified channel.

    Raises:
        HTTPException: If any error occurs during the measurement.
    """
    try:
        current_measurement = device.measure_current(channel)
        return {"channel": channel, "current": current_measurement}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.get("/measure-voltage/")
def measure_voltage(
    channel: int = Query(
        default=1, description="Channel number to measure voltage from"
    )
):
    """
    Measures the voltage from a specified channel.

    Args:
        channel (int, optional): The channel number from which to measure voltage. Defaults to 1.

    Returns:
        dict: The voltage measurement for the specified channel.

    Raises:
        HTTPException: If any error occurs during the measurement.
    """
    try:
        voltage_measurement = device.measure_voltage(channel)
        return {"channel": channel, "voltage": voltage_measurement}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
