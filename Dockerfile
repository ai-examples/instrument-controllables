# Base stage for common setup
FROM python:3 as base

# Set the working directory in the container
WORKDIR /app

# Copy the common directories and files into the container
COPY base/ ./base/
COPY requirements.txt ./
COPY 30-usbtmc.rules /etc/udev/rules.d/

# Install necessary packages to access USB VISA device
RUN apt-get update && \
    apt-get install -y \
    libusb-1.0-0 \
    udev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Stage for DL3021 service
FROM base as dl3021

# Copy the DL3021 specific directory into the container
COPY rigol_dl3021/ ./rigol_dl3021/

# Command to run the DL3021 app
CMD ["uvicorn", "rigol_dl3021.web_app:app", "--host", "0.0.0.0", "--port", "8002"]

# Stage for DP832 service
FROM base as dp832

# Copy the DP832 specific directory into the container
COPY rigol_dp832/ ./rigol_dp832/

# Command to run the DP832 app
CMD ["uvicorn", "rigol_dp832.web_app:app", "--host", "0.0.0.0", "--port", "8001"]