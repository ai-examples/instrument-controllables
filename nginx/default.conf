# Configuration for HTTP server
server {
    listen 80;  # Listen on port 80
    server_name amahpour.servehttp.com;  # Set the server name

    location / {
        return 301 https://$host$request_uri;  # Redirect all traffic to use SSL
    }
}

# Configuration for HTTPS server
server {
    listen 443 ssl;  # Listen on port 443 with SSL
    server_name amahpour.servehttp.com;  # Set the server name

    ssl_certificate /etc/ssl/certs/fullchain.pem;  # Set the SSL certificate file
    ssl_certificate_key /etc/ssl/private/privkey.pem;  # Set the SSL private key file

    location /dp832/ {
        proxy_pass http://dp832:8001/;  # Proxy requests to the dp832 service
        proxy_set_header Host $host;  # Set the Host header for the proxy
        proxy_set_header X-Real-IP $remote_addr;  # Set the X-Real-IP header for the proxy
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;  # Set the X-Forwarded-For header for the proxy
        proxy_set_header X-Forwarded-Proto $scheme;  # Set the X-Forwarded-Proto header for the proxy
    }

    # Route /dl3021 to dl3021 service
    location /dl3021/ {
        proxy_pass http://dl3021:8002/;  # Proxy requests to the dl3021 service
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    # Openapi.yaml serve
    location /openapi.yaml {
        alias /etc/nginx/openapi/openapi.yaml;  # Path to the openapi.yaml file
    }

    # Default route for server status
    location / {
        default_type application/json;  # Set the content type to JSON
        return 200 '{"status": "ok"}';  # Return a 200 status code with JSON content
    }
}