import logging
import usbtmc
import pyvisa
from typing import Literal

# Set up basic logging config
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    force=True,
)


class CommBase:
    # Define the valid USB connection types as a class-level attribute
    USBConnType = Literal["USBTMC", "VISA", "Socket"]

    def __init__(
        self,
        usb_conn_type: USBConnType,
        vid: int = None,
        pid: int = None,
        visa_resource_prefix: str = None,
    ):
        """
        Initialize the Rigol Programmable Power Supply

        :param usb_conn_type: USB Connection type (See VALID_USB_CONN_TYPES for list of valid inputs)
        :type usb_conn_type: USBConnType

        :param vid: Vendor ID (Decimal format or "0x0000" format for hex)
        :type vid: int

        :param pid: Product ID (Decimal format or "0x0000" format for hex)
        :type pid: int

        :param device_path: Device path for Socket connection (example: /dev/usbtmc0)
        :type device_path: str
        """

        # Check for USB connection type and set up device accordingly
        self.visa_resource_prefix = visa_resource_prefix
        self.usb_conn_type = usb_conn_type
        if usb_conn_type == "USBTMC":
            self.configure_usbtmc(vid, pid)
        elif usb_conn_type == "VISA":
            self.configure_visa(vid, pid)
        elif usb_conn_type == "Socket":
            pass
        else:
            # Runtime check to enforce the allowed values
            raise ValueError(
                f"Invalid USB connection type: {usb_conn_type}. Valid types are {self.VALID_USB_CONN_TYPES}"
            )

    def configure_usbtmc(self, vid: int, pid: int):
        self.inst = usbtmc.Instrument(vid, pid)

    def configure_visa(self, vid: int, pid: int):
        self.rm = pyvisa.ResourceManager()
        instrument_list = self.rm.list_resources()
        logging.debug(f"instruments: {instrument_list}")

        # Find the device based off the VID and PID
        visa_address = self.find_visa_resource(
            vid, pid, instrument_list, prefix=self.visa_resource_prefix
        )
        if visa_address is not None:
            self.inst = self.rm.open_resource(visa_address, read_termination="\n")
        else:
            # No device name specified. Raise and error
            raise IOError(
                f'No VISA devices found using vid "{vid}" and pid "{pid}" but the following VISA devices have been found: {instrument_list}'
            )

    @staticmethod
    def find_visa_resource(
        vid: int, pid: int, resource_strings: list, prefix: str
    ) -> str:
        """
        Finds a VISA resource string that matches the given VID, PID, and a specified prefix in the resource string.
        This function iterates through a list of VISA resource strings and returns the first one that matches the given VID, PID, and prefix.
        The match is checked against both the hexadecimal and decimal representations of the VID and PID, along with the specified prefix in the resource string.

        :param vid: Vendor ID in decimal format.
        :type vid: int
        :param pid: Product ID in decimal format.
        :type pid: int
        :param resource_strings: List of VISA resource strings to search through.
        :type resource_strings: list
        :param prefix: The prefix that the resource string must start with after the initial identifier (e.g., "DP8").
        :type prefix: str
        :return: The matching VISA resource string, or None if no match is found.
        :rtype: str or None
        """
        hex_vid, hex_pid = f"0x{vid:X}", f"0x{pid:X}"
        dec_vid, dec_pid = str(vid), str(pid)

        for resource in resource_strings:
            # Extract the part of the resource string that should contain the prefix
            parts = resource.split("::")
            if len(parts) >= 4:
                serial_and_more = parts[
                    3
                ]  # Assuming the serial/model part is the third section
                if (
                    any(x in resource for x in (hex_vid, dec_vid))
                    and any(x in resource for x in (hex_pid, dec_pid))
                    and serial_and_more.startswith(prefix)
                ):
                    return resource

        return None

    def query_device(self, command: str) -> str:
        """
        Wrapper function to query the device.

        :param command: Command to send to the device
        :type command: str
        :return: Response from the device
        :rtype: str
        """
        if self.usb_conn_type == "USBTMC":
            return self.inst.ask(command)
        elif self.usb_conn_type == "VISA":
            return self.inst.query(command)
        else:
            raise NotImplementedError(
                f"Query method for {self.usb_conn_type} not found."
            )

    def write_device(self, command: str):
        """
        Send a write command to the device.

        :param command: SCPI command to be sent to the device.
        """
        self.inst.write(command)

    def close(self):
        """
        Close the opened DP832 device and the VISA resource manager
        """
        self.inst.close()
        if self.usb_conn_type == "VISA":
            self.rm.close()

    def id(self) -> dict:
        """
        Query the ID string of the instrument

        :return: Dictionary containing the manufacturer, instrument model,
                 serial number, and version number.
        :rtype: dict
        """
        id_str = self.query_device("*IDN?").strip().split(",")
        logging.debug(f"id_str: {id_str}")
        return {
            "manufacturer": id_str[0],
            "model": id_str[1],
            "serial_number": id_str[2],
            "version": id_str[3],
        }


if __name__ == "__main__":
    test_inst = CommBase(
        usb_conn_type="VISA", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DP8"
    )
    logging.info(test_inst.id())
    test_inst.close()

    test_inst = CommBase(
        usb_conn_type="VISA", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DL3"
    )
    logging.info(test_inst.id())
    test_inst.close()

    test_inst = CommBase(
        usb_conn_type="USBTMC", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DP8"
    )
    logging.info(test_inst.id())
    test_inst.close()

    test_inst = CommBase(
        usb_conn_type="USBTMC", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DL3"
    )
    logging.info(test_inst.id())
    test_inst.close()
