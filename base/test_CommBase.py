import pytest
from unittest.mock import MagicMock, patch, create_autospec
from base.CommBase import CommBase, pyvisa, usbtmc


class TestCommBase:
    @pytest.mark.parametrize("conn_type", ["USBTMC", "VISA", "Socket"])
    def test_init_valid_connection_types(self, conn_type):
        with patch(
            "base.CommBase.usbtmc.Instrument", autospec=True
        ) as mock_usbtmc, patch(
            "base.CommBase.pyvisa.ResourceManager", autospec=True
        ) as mock_pyvisa, patch(
            "base.CommBase.CommBase.find_visa_resource",
            return_value="USB0::0x1234::0x5678::INSTR",
        ) as mock_find_visa_resource:
            mock_usbtmc.return_value = MagicMock()
            mock_pyvisa_instance = mock_pyvisa.return_value
            mock_pyvisa_instance.list_resources.return_value = []

            if conn_type == "VISA":
                mock_find_visa_resource.return_value = "USB0::0x1234::0x5678::INSTR"

            device = CommBase(usb_conn_type=conn_type, vid=123, pid=456)

            assert device.usb_conn_type == conn_type
            if conn_type == "USBTMC":
                mock_usbtmc.assert_called_once_with(123, 456)
            elif conn_type == "VISA":
                mock_pyvisa.assert_called_once()
                mock_find_visa_resource.assert_called_once()

    def test_init_invalid_connection_type(self):
        with pytest.raises(ValueError):
            CommBase(usb_conn_type="INVALID", vid=123, pid=456)

    @pytest.mark.parametrize(
        "conn_type,expected_method", [("USBTMC", "ask"), ("VISA", "query")]
    )
    def test_query_device(self, conn_type, expected_method):
        command = "*IDN?"
        expected_response = "Mocked Response"

        with patch(
            "base.CommBase.usbtmc.Instrument", autospec=True
        ) as mock_usbtmc, patch(
            "base.CommBase.pyvisa.ResourceManager", autospec=True
        ) as mock_pyvisa, patch(
            "base.CommBase.CommBase.find_visa_resource",
            return_value="USB0::0x1234::0x5678::INSTR",
        ):
            mock_inst = MagicMock()
            mock_usbtmc.return_value = mock_inst
            mock_pyvisa_instance = mock_pyvisa.return_value
            mock_pyvisa_instance.open_resource.return_value = mock_inst

            mock_inst.ask.return_value = expected_response
            mock_inst.query.return_value = expected_response

            device = CommBase(usb_conn_type=conn_type, vid=123, pid=456)
            response = device.query_device(command)

            assert response == expected_response
            getattr(mock_inst, expected_method).assert_called_once_with(command)

    def test_id_method(self):
        expected_id_str = "Manufacturer,Model,Serial,Version"
        # Patch both the query_device method and usbtmc.Instrument to prevent real device interactions
        with patch(
            "base.CommBase.CommBase.query_device", return_value=expected_id_str
        ), patch("base.CommBase.usbtmc.Instrument") as mock_usbtmc:
            mock_usbtmc.return_value = (
                MagicMock()
            )  # Mock the usbtmc.Instrument to prevent initialization errors

            device = CommBase(usb_conn_type="USBTMC", vid=123, pid=456)
            id_info = device.id()

            assert id_info == {
                "manufacturer": "Manufacturer",
                "model": "Model",
                "serial_number": "Serial",
                "version": "Version",
            }

    def test_configure_visa_device_not_found(self):
        vid = 1234
        pid = 5678
        resource_strings = ["INSTR::OTHER::123::MYSERIAL::0"]
        with patch("base.CommBase.pyvisa.ResourceManager") as mock_pyvisa:
            mock_rm = MagicMock()
            mock_pyvisa.return_value = mock_rm
            mock_rm.list_resources.return_value = resource_strings

            with pytest.raises(IOError):
                device = CommBase(
                    usb_conn_type="VISA",
                    vid=vid,
                    pid=pid,
                    visa_resource_prefix="MYSERIAL",
                )
