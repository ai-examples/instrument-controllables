# Remote Lab Instrument Control via OpenAI GPT Actions
A repository for controlling lab instruments remotely using custom OpenAI GPT Actions.

## Overview
This project enables remote control and automation of lab instruments via a web interface and OpenAI's GPT Actions. The components include:

| Component        | Description                                                                                                           |
|------------------|-----------------------------------------------------------------------------------------------------------------------|
| Web Applications | Python-based web applications serve as an API for controlling lab instruments, allowing external services like ChatGPT to interface with lab hardware. |
| Nginx            | A web server for SSL/TLS management, routing HTTPS traffic to the correct web applications, and serving the `openapi.yaml` file for API documentation. |
| Docker Compose   | Manages multi-container Docker applications, linking the web application and Nginx to simplify network configuration and deployment. |
| openapi.yaml     | The OpenAPI Schema detailing the API for GPT Actions, facilitating the interpretation of commands and data exchange with the server. |
| SSL Certificate  | Required for HTTPS setup. These private files (e.g., keys and certificates) are essential for secure communication but are excluded from the repository to maintain security. |

## Configuration
### Getting a domain name
Obtaining a domain name is essential for accessing your server over the internet. While there are numerous methods to acquire a domain name, one straightforward option is using a Dynamic DNS service like No-ip.org. This service can map a domain name to your home IP address, ensuring you create an A record for proper routing.

### SSL certificate using Let's Encrypt
Secure communication is mandatory for interacting with ChatGPT Actions, necessitating an SSL certificate. You can use Certbot to obtain a certificate with the following command (ensure port 80 is free):
```
sudo certbot certonly --standalone --agree-tos -v --preferred-challenges http -d amahpour.servehttp.com
```
Store the generated files in a `certbot` directory within this repository. Here are the essential files:
| File           | Description                                                   |
|----------------|---------------------------------------------------------------|
| `privkey.pem`  | The private key for your certificate, crucial for decryption. |
| `fullchain.pem`| The certificate file, widely supported by server software.    |
| `chain.pem`    | Required for OCSP stapling in Nginx versions 1.3.7 or later.  |
| `cert.pem`     | Seldom used directly; refer to further documentation before use. |

**Note: Never share these files publicly. They are excluded from the repository via `.gitignore` for security.**

### Configuring Nginx
Post-certificate acquisition, configure Nginx to use SSL and direct web traffic appropriately. Modify the `nginx/default.conf` file, replacing `amahpour.servehttp.com` with your domain name. This step ensures HTTPS traffic is handled correctly and routes to your web applications.

### Setting up GPT Action
For the GPT Action to communicate with your server, it needs the OpenAPI schema defined in `openapi.yaml`. Replace `amahpour.servehttp.com` in this file with your domain name and upload the schema to the "Actions" section in your custom GPT settings.

## Running the Application
Complete the configuration steps above, then deploy your server with:
```
docker compose up
```
Verify external accessibility (e.g., using a mobile device with mobile data) and finalize by testing the GPT Action integration through your custom GPT settings. Hit the "Test" button under the available actions. As you hit different routes you should see the different containers and Nginx reporting status of HTTP requests from OpenAI's servers.

## Detailed Web Application Routing

This project utilizes Nginx to route requests to various web applications, each serving a specific function in the control and monitoring of lab instruments. Below is a breakdown of the routing configuration and the purpose of each route within the Nginx setup:

### HTTPS Redirection
- All HTTP traffic on port 80 is redirected to HTTPS, ensuring secure communication. This is crucial for protecting the data integrity and privacy of the instrument control commands.

### Instrument Control Services
- **DP832 Power Supply Control (`/dp832/`):** This route proxies requests to the DP832 service, which manages the DP832 programmable power supply. The service listens on port 8001 and allows for remote control and status monitoring of the power supply via the web interface or GPT actions.

- **DL3021 Electronic Load Control (`/dl3021/`):** Similar to the DP832 service, this route directs requests to the DL3021 service for controlling the DL3021 electronic load device. It operates on port 8002, enabling users to manage load settings and read device statuses remotely.

### OpenAPI Documentation
- The `/openapi.yaml` route serves the OpenAPI schema directly from Nginx. This file is crucial for defining the API interface that GPT Actions use to interact with the web applications. By serving this file, users can easily access and review the API documentation.

### Server Status Endpoint
- A default route (`/`) returns a simple JSON response indicating the server's operational status. This endpoint is useful for quick health checks and monitoring of the server's availability.

Each of these routes is configured to ensure secure, efficient, and clear communication between the client, Nginx, and the respective web applications. This setup facilitates a modular approach, allowing for easy updates and maintenance of individual services without impacting the overall system's functionality.