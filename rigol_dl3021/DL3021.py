from base.CommBase import CommBase


class DL3021(CommBase):
    """
    Rigol DL3021 Electronic Load Command Wrapper.

    This class provides a high-level interface for controlling a Rigol DL3021
    electronic load. It wraps SCPI commands into Python methods for ease of use.
    """

    def measure_voltage(self) -> float:
        """
        Measure and return the voltage.

        :return: Voltage in volts.
        """
        return float(self.query_device(":MEAS:VOLT?").partition("\n")[0])

    def measure_current(self) -> float:
        """
        Measure and return the current.

        :return: Current in amperes.
        """
        return float(self.query_device(":MEAS:CURR?").partition("\n")[0])

    def measure_power(self) -> float:
        """
        Measure and return the power.

        :return: Power in watts.
        """
        return float(self.query_device(":MEAS:POW?").partition("\n")[0])

    def measure_resistance(self) -> float:
        """
        Measure and return the resistance.

        :return: Resistance in ohms.
        """
        return float(self.query_device(":MEAS:RES?").partition("\n")[0])

    def set_cc_slew_rate(self, slew: float):
        """
        Set the current slew rate in CC mode.

        :param slew: Slew rate in A/us.
        """
        self.write_device(f":SOURCE:CURRENT:SLEW {slew}")

    def is_enabled(self) -> bool:
        """
        Check if the electronic load is enabled.

        :return: True if enabled, False otherwise.
        """
        return self.query_device(":SOURCE:INPUT:STAT?") == "1"

    def enable(self):
        """
        Enable the electronic load.
        """
        self.write_device(":SOURCE:INPUT:STAT ON")

    def disable(self):
        """
        Disable the electronic load.
        """
        self.write_device(":SOURCE:INPUT:STAT OFF")

    def set_mode(self, mode: str):
        """
        Set the operation mode of the load.

        :param mode: Load mode (e.g., "Current", "Voltage", "Power", "Resistance").
        """
        self.write_device(f":SOURCE:FUNCTION {mode}")

    def query_mode(self) -> str:
        """
        Query the current operation mode of the load.

        :return: Operation mode as a string.
        """
        return self.query_device(":SOURCE:FUNCTION?")

    def set_cc_current(self, current: float):
        """
        Set the constant current (CC) mode current limit.

        :param current: Current limit in amperes.
        """
        self.write_device(f":SOURCE:CURRENT:LEV:IMM {current}")

    def set_cp_power(self, power: float):
        """
        Set the constant power (CP) mode power limit.

        :param power: Power limit in watts.
        """
        self.write_device(f":SOURCE:POWER:LEV:IMM {power}")

    def set_cp_ilim(self, ilim: float):
        """
        Set the constant power (CP) mode current limit.

        :param ilim: Current limit in amperes.
        """
        self.query_device(f":SOURCE:POWER:ILIM {ilim}")

    def cc_mode(self, current: float, activate: bool = True):
        """
        Configure and optionally activate constant-current mode.

        :param current: Current setting in amperes.
        :param activate: If True, enable the load after setting the current.
        """
        self.set_mode("Current")
        self.set_cc_current(current)
        if activate:
            self.enable()

    def cp_mode(self, power: float, activate: bool = True):
        """
        Configure and optionally activate constant-power mode.

        :param power: Power setting in watts.
        :param activate: If True, enable the load after setting the power.
        """
        self.set_mode("Power")
        self.set_cp_power(power)
        if activate:
            self.enable()

    def reset(self):
        """
        Reset the device to its default settings.
        """
        self.write_device("*RST")
