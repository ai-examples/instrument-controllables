# Append parent path for includes
import os, sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

class ModeSettings(BaseModel):
    mode: str  # Modes could be "Current", "Voltage", "Power", "Resistance"

class CurrentSettings(BaseModel):
    current: float
    activate: bool = True

class PowerSettings(BaseModel):
    power: float
    activate: bool = True

class SlewRateSettings(BaseModel):
    slew_rate: float

from rigol_dl3021.DL3021 import DL3021

app = FastAPI()

# Initialize the DL3021 connection once and reuse it across requests
device = DL3021(usb_conn_type="VISA", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DL3")

@app.get("/")
def read_root():
    """
    Root endpoint for basic API health check.
    Returns:
        dict: A simple status message indicating the API is operational.
    """
    return {"status": "OK"}

@app.get("/measure/voltage/")
def measure_voltage():
    """
    Measures the voltage.
    Returns:
        float: The measured voltage in volts.
    """
    try:
        return {"voltage": device.measure_voltage()}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/measure/current/")
def measure_current():
    """
    Measures the current.
    Returns:
        float: The measured current in amperes.
    """
    try:
        return {"current": device.measure_current()}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/measure/power/")
def measure_power():
    """
    Measures the power.
    Returns:
        float: The measured power in watts.
    """
    try:
        return {"power": device.measure_power()}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/measure/resistance/")
def measure_resistance():
    """
    Measures the resistance.
    Returns:
        float: The measured resistance in ohms.
    """
    try:
        return {"resistance": device.measure_resistance()}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/control/enable/")
def enable_load():
    """
    Enables the electronic load.
    """
    try:
        device.enable()
        return {"message": "Electronic load enabled"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/control/disable/")
def disable_load():
    """
    Disables the electronic load.
    """
    try:
        device.disable()
        return {"message": "Electronic load disabled"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/set-mode/")
def set_mode(settings: ModeSettings):
    """
    Sets the operation mode of the load.
    """
    try:
        device.set_mode(settings.mode)
        return {"message": f"Mode set to {settings.mode}"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/query-mode/")
def query_mode():
    """
    Queries the current operation mode of the load.
    """
    try:
        mode = device.query_mode()
        return {"mode": mode}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/set-cc-current/")
def set_cc_current(settings: CurrentSettings):
    """
    Sets the constant current mode and current limit.
    """
    try:
        device.cc_mode(settings.current, settings.activate)
        return {"message": "Constant current mode set successfully"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/set-cp-power/")
def set_cp_power(settings: PowerSettings):
    """
    Sets the constant power mode and power limit.
    """
    try:
        device.cp_mode(settings.power, settings.activate)
        return {"message": "Constant power mode set successfully"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/set-slew-rate/")
def set_slew_rate(settings: SlewRateSettings):
    """
    Sets the current slew rate in CC mode.
    """
    try:
        device.set_cc_slew_rate(settings.slew_rate)
        return {"message": f"Slew rate set to {settings.slew_rate} A/us"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/reset/")
def reset_device():
    """
    Resets the device to its default settings.
    """
    try:
        device.reset()
        return {"message": "Device reset to default settings"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
