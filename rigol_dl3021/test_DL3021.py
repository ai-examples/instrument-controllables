import pytest

# Assuming the DL3021 class is in a module named rigol_dl
from rigol_dl3021.DL3021 import DL3021


class TestDL3021:
    """
    Contains tests for the Rigol DL3021 electronic load to ensure its functionality
    such as measurement capabilities, mode setting, and output enabling/disabling operates as expected.
    """

    @classmethod
    def setup_class(cls):
        """
        Prepares the testing environment, potentially including opening the connection to the device.

        This method is called before starting the tests to ensure a consistent starting state.
        """
        # Example setup code (adjust as necessary for your setup)
        cls.load = DL3021(
            usb_conn_type="VISA", vid=0x1AB1, pid=0x0E11, visa_resource_prefix="DL3"
        )

    @classmethod
    def teardown_class(cls):
        """
        Cleans up the testing environment, potentially including closing the connection to the device.

        This method is called after all tests have run to ensure the device is left in a safe state.
        """
        # Example teardown code (adjust as necessary for your teardown)
        cls.load.disable()
        cls.load.reset()
        cls.load.close()

    def test_measure_voltage(self):
        """
        Verifies that the load can measure voltage accurately.
        """
        # Example test (implement based on your testing needs and environment)
        expected_max_voltage = 0.01
        measured_voltage = self.load.measure_voltage()
        assert expected_max_voltage > measured_voltage

    def test_enable_disable(self):
        """
        Tests enabling and disabling the load.
        """
        self.load.disable()
        assert not self.load.is_enabled()
        self.load.enable()
        assert self.load.is_enabled()

    def test_measure_current(self):
        """
        Verifies that the load can measure current accurately.
        """
        expected_max_current = 0.01
        measured_current = self.load.measure_current()
        assert expected_max_current > measured_current

    def test_measure_power(self):
        """
        Tests the load's ability to measure power.
        """
        expected_max_power = 0.01
        measured_power = self.load.measure_power()
        assert expected_max_power > measured_power

    def test_measure_resistance(self):
        """
        Ensures that the load can measure resistance correctly.
        """
        expected_max_resistance = 0.01
        measured_resistance = self.load.measure_resistance()
        assert expected_max_resistance > measured_resistance

    def test_set_cc_slew_rate(self):
        """
        Tests setting the current slew rate in CC mode.
        """
        self.load.set_cc_slew_rate(0.1)  # Example slew rate in A/us
        # Assuming there's a way to verify the slew rate, use that here
        # This test might depend on your ability to query the current slew rate setting from the device

    def test_set_mode(self):
        """
        Verifies the load's ability to switch between different modes.
        """
        self.load.set_mode("Current")
        assert "CC" == self.load.query_mode()
        self.load.set_mode("Voltage")
        assert "CV" == self.load.query_mode()
        # Add tests for other modes as applicable

    def test_cc_mode_activation(self):
        """
        Tests the convenience method for activating CC mode with a specified current.
        """
        self.load.cc_mode(1.0)  # Setting 1 A current in CC mode
        assert self.load.is_enabled()
        assert "CC" == self.load.query_mode()
        # Verify the set current if possible

    def test_cp_mode_activation(self):
        """
        Tests the convenience method for activating CP mode with a specified power.
        """
        self.load.cp_mode(10)  # Setting 10 W power in CP mode
        assert self.load.is_enabled()
        assert "CP" == self.load.query_mode()
        # Verify the set power if possible
