from fastapi.testclient import TestClient
from rigol_dl3021.web_app import app

client = TestClient(app)

class TestDL3021API:
    def test_read_root(self):
        response = client.get("/")
        assert response.status_code == 200
        assert response.json() == {"status": "OK"}

    def test_measure_voltage(self):
        response = client.get("/measure/voltage/")
        assert response.status_code == 200
        assert "voltage" in response.json()

    def test_measure_current(self):
        response = client.get("/measure/current/")
        assert response.status_code == 200
        assert "current" in response.json()

    def test_measure_power(self):
        response = client.get("/measure/power/")
        assert response.status_code == 200
        assert "power" in response.json()

    def test_measure_resistance(self):
        response = client.get("/measure/resistance/")
        assert response.status_code == 200
        assert "resistance" in response.json()

    def test_enable_load(self):
        response = client.post("/control/enable/")
        assert response.status_code == 200
        assert response.json() == {"message": "Electronic load enabled"}

    def test_disable_load(self):
        response = client.post("/control/disable/")
        assert response.status_code == 200
        assert response.json() == {"message": "Electronic load disabled"}

    def test_set_mode(self):
        response = client.post("/set-mode/", json={"mode": "Current"})
        assert response.status_code == 200
        assert "Mode set to Current" in response.json()["message"]

    def test_query_mode(self):
        response = client.get("/query-mode/")
        assert response.status_code == 200
        # This assumes the mode has been previously set in the tests or is set to a default
        assert "mode" in response.json()

    def test_set_cc_current(self):
        response = client.post("/set-cc-current/", json={"current": 0.5, "activate": True})
        assert response.status_code == 200
        assert response.json() == {"message": "Constant current mode set successfully"}

    def test_set_cp_power(self):
        response = client.post("/set-cp-power/", json={"power": 10.0, "activate": True})
        assert response.status_code == 200
        assert response.json() == {"message": "Constant power mode set successfully"}

    def test_set_slew_rate(self):
        response = client.post("/set-slew-rate/", json={"slew_rate": 0.1})
        assert response.status_code == 200
        assert "Slew rate set to" in response.json()["message"]

    def test_reset_device(self):
        response = client.post("/reset/")
        assert response.status_code == 200
        assert response.json() == {"message": "Device reset to default settings"}
